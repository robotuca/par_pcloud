 /*
  * PCL Example using ROS and CPP
  */

 // Include the ROS library
 #include <ros/ros.h>

 // Include pcl
 #include <pcl_conversions/pcl_conversions.h>
 #include <pcl/point_cloud.h>
// #include <pcl/point_types.h>
 #include <pcl/filters/voxel_grid.h>
 
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/features/normal_3d.h>
#include <pcl/filters/passthrough.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/conversions.h>
#include <pcl/common/centroid.h>

 // Include PointCloud2 message
 #include <sensor_msgs/PointCloud2.h>
 
 // Include Marker message
 #include <visualization_msgs/Marker.h>

 // Topics
 static const std::string IMAGE_TOPIC = "/camera/depth/points";
 static const std::string PUBLISH_TOPIC = "/pcl/points";

 // ROS Publisher
 ros::Publisher pub;
 ros::Publisher pub_marker;
 
 typedef pcl::PointXYZ PointT;

 void cloud_cb(const sensor_msgs::PointCloud2ConstPtr& cloud_msg)
 {
    // Container for original & filtered data
    pcl::PCLPointCloud2* cloud = new pcl::PCLPointCloud2;
    pcl::PCLPointCloud2ConstPtr cloudPtr(cloud);
    pcl::PCLPointCloud2 cloud2_filtered;

    // Convert to PCL data type
    pcl_conversions::toPCL(*cloud_msg, *cloud);

     
    // Convert PointCloud2 to PointCloud
    pcl::PointCloud<PointT>::Ptr cloud_filtered (new pcl::PointCloud<PointT>);
    pcl::fromPCLPointCloud2( *cloudPtr, *cloud_filtered);
     
    // Perform the actual filtering
    pcl::VoxelGrid<PointT> sor;
    sor.setInputCloud (cloud_filtered);
    sor.setLeafSize (0.002, 0.002, 0.002);
    sor.filter (*cloud_filtered);
     
     
    // Build a passthrough filter
    pcl::PassThrough<PointT> pass;
    pass.setInputCloud (cloud_filtered);
    pass.setFilterFieldName ("z");
    pass.setFilterLimits (0, 1.2);
    pass.filter (*cloud_filtered);
    std::cerr << "PointCloud after filtering has: " << cloud_filtered->size () << " data points." << std::endl;
          
          
    // Estimate point normals
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);
    pcl::search::KdTree<PointT>::Ptr tree (new pcl::search::KdTree<PointT> ());
    pcl::NormalEstimation<PointT, pcl::Normal> ne;
    
    ne.setSearchMethod (tree);
    ne.setInputCloud (cloud_filtered);
    ne.setKSearch (50);
    ne.compute (*cloud_normals);
          
    // Create the segmentation object for the planar model and set all the parameters
    pcl::SACSegmentationFromNormals<PointT, pcl::Normal> seg; 
    pcl::ExtractIndices<PointT> extract;
    pcl::ExtractIndices<pcl::Normal> extract_normals;
    pcl::ModelCoefficients::Ptr coefficients_plane (new pcl::ModelCoefficients), coefficients_cylinder (new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers_plane (new pcl::PointIndices), inliers_cylinder (new pcl::PointIndices);

    seg.setOptimizeCoefficients (true);
    seg.setModelType (pcl::SACMODEL_NORMAL_PLANE);
    seg.setNormalDistanceWeight (0.1);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setMaxIterations (100);
    seg.setDistanceThreshold (0.05);
    seg.setInputCloud (cloud_filtered);
    seg.setInputNormals (cloud_normals);
    
    // Obtain the plane inliers and coefficients
    seg.segment (*inliers_plane, *coefficients_plane);
    std::cerr << "Plane coefficients: " << *coefficients_plane << std::endl;

    // Extract the planar inliers from the input cloud
    extract.setInputCloud (cloud_filtered);
    extract.setIndices (inliers_plane);
    //extract.setNegative (false);
    //extract.filter (*cloud_filtered);

    // Remove the planar inliers, extract the rest
    extract.setNegative (true);
    extract.filter (*cloud_filtered);
    extract_normals.setNegative (true);
    extract_normals.setInputCloud (cloud_normals);
    extract_normals.setIndices (inliers_plane);
    extract_normals.filter (*cloud_normals);

    // Create the segmentation object for cylinder segmentation and set all the parameters
    seg.setOptimizeCoefficients (true);
    seg.setModelType (pcl::SACMODEL_CYLINDER);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setNormalDistanceWeight (0.1);
    seg.setMaxIterations (10000);
    seg.setDistanceThreshold (0.05);
    seg.setRadiusLimits (0.02, 0.2);
    seg.setInputCloud (cloud_filtered);
    seg.setInputNormals (cloud_normals);

    // Obtain the cylinder inliers and coefficients
    seg.segment (*inliers_cylinder, *coefficients_cylinder);
    std::cerr << "Cylinder coefficients: " << *coefficients_cylinder << std::endl;
     
    extract.setInputCloud (cloud_filtered);
    extract.setIndices (inliers_cylinder);
    extract.setNegative (false);
    extract.filter (*cloud_filtered);

     
    
    // Create a marker to visualize in RVIZ
    visualization_msgs::Marker my_marker;
    my_marker.header.frame_id = "camera_depth_optical_frame";
    my_marker.header.stamp = ros::Time();
    my_marker.id = 0;
    my_marker.type = visualization_msgs::Marker::SPHERE;
    my_marker.action = visualization_msgs::Marker::ADD;
    my_marker.pose.position.x = coefficients_cylinder->values[0];
    my_marker.pose.position.y = coefficients_cylinder->values[1];
    my_marker.pose.position.z = coefficients_cylinder->values[2];
    my_marker.pose.orientation.x = 0.0;
    my_marker.pose.orientation.y = 0.0;
    my_marker.pose.orientation.z = 0.0;
    my_marker.pose.orientation.w = 1.0;
    my_marker.scale.x = coefficients_cylinder->values[6]*2;
    my_marker.scale.y = coefficients_cylinder->values[6]*2;
    my_marker.scale.z = coefficients_cylinder->values[6]*2;
    my_marker.color.a = 1.0; // Don't forget to set the alpha!
    my_marker.color.r = 0.0;
    my_marker.color.g = 1.0;
    my_marker.color.b = 0.0;

  
    
    // Convert to ROS data type
    pcl::toPCLPointCloud2(*cloud_filtered, cloud2_filtered);
    sensor_msgs::PointCloud2 output;
    pcl_conversions::moveFromPCL(cloud2_filtered, output);

    // Publish the output pointCloud
    pub.publish (output);
     
    // Publish the marker
    pub_marker.publish(my_marker);

 }

 int main (int argc, char** argv)
 {
     // Initialize the ROS Node "roscpp_pcl_example"
     ros::init (argc, argv, "roscpp_pcl_example");
     ros::NodeHandle nh;

     // Print "Hello" message with node name to the terminal and ROS log file
     ROS_INFO_STREAM("Hello from ROS Node: " << ros::this_node::getName());

     // Create a ROS Subscriber to IMAGE_TOPIC with a queue_size of 1 and a callback function to cloud_cb
     ros::Subscriber sub = nh.subscribe(IMAGE_TOPIC, 1, cloud_cb);

     // Create a ROS publisher to PUBLISH_TOPIC with a queue_size of 1
     pub = nh.advertise<sensor_msgs::PointCloud2>(PUBLISH_TOPIC, 1);

     pub_marker = nh.advertise<visualization_msgs::Marker>("my_marker", 1);
     // Spin
     ros::spin();

     // Success
     return 0;
 }
