 /*
  * PCL Example using ROS and CPP
  */

 // Include the ROS library
 #include <ros/ros.h>

 // Include pcl
 #include <pcl_conversions/pcl_conversions.h>
 #include <pcl/point_cloud.h>
// #include <pcl/point_types.h>
 #include <pcl/filters/voxel_grid.h>
 
#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/features/normal_3d.h>
#include <pcl/filters/passthrough.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/conversions.h>
#include <pcl/common/centroid.h>

 // Include PointCloud2 message
 #include <sensor_msgs/PointCloud2.h>
 
 #include <visualization_msgs/Marker.h>
#include<std_msgs/String.h>

// Format
#include <pcl/io/ply_io.h>

 // Topics
 static const std::string PATH_TOPIC = "/cloud/path";
 static const std::string PUBLISH_TOPIC = "/pcl/points_file";

 // ROS Publisher
 ros::Publisher pub;
 ros::Publisher pub_marker;
 
 typedef pcl::PointXYZ PointT;

 void cloud_cb(const std_msgs::String::ConstPtr& msg)
 {
     // Container for original & filtered data
    typename pcl::PointCloud<PointT>::Ptr cloud_input;
    cloud_input=typename pcl::PointCloud<PointT>::Ptr(new typename pcl::PointCloud<PointT>);       
    ROS_INFO_STREAM("Loading... " << msg->data.c_str());
     if (pcl::io::loadPLYFile<PointT> (msg->data.c_str(), *cloud_input) == -1) //* load the file
     {
        PCL_ERROR ("Couldn't read file \n");
        
     }
     else{
        // Convert to ROS data type
         sensor_msgs::PointCloud2 output;
         //pcl_conversions::moveFromPCL(*cloud_input, output);
         pcl::toROSMsg(*cloud_input.get(),output );
         output.header.frame_id="map";
         // Publish the data
         pub.publish (output);
     }
     

 }

 int main (int argc, char** argv)
 {
     // Initialize the ROS Node "roscpp_pcl_example"
     ros::init (argc, argv, "roscpp_pcl_load_example");
     ros::NodeHandle nh;

     // Print "Hello" message with node name to the terminal and ROS log file
     ROS_INFO_STREAM("Hello from ROS load cloud Node: " << ros::this_node::getName());

     // Create a ROS Subscriber to cloud PATH_TOPIC with a queue_size of 1 and a callback function to cloud_cb
     ros::Subscriber sub = nh.subscribe(PATH_TOPIC, 1, cloud_cb);

     // Create a ROS publisher to PUBLISH_TOPIC with a queue_size of 1
     pub = nh.advertise<sensor_msgs::PointCloud2>(PUBLISH_TOPIC, 1);

     // Spin
     ros::spin();

     // Success
     return 0;
 }
